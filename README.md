# Swanalekha WebExtension

Swanalekha WebExtension is a plugin for Mozilla Firefox and Google Chrome/Chromium, which enables Swanalekha, the trasliteration based malayalam input tools, on the text fields.

This plugin uses the [Swanalekha-JS](https://gitlab.com/smc/swanalekha/tree/master/js) library written by Santhosh Thottingal which is released under MIT License.

# Firefox
https://addons.mozilla.org/en-US/firefox/addon/swanalekha-ml/

# Chrome
https://chrome.google.com/webstore/detail/%E0%B4%B8%E0%B5%8D%E0%B4%B5%E0%B4%A8%E0%B4%B2%E0%B5%87%E0%B4%96-swanalekha/najmphaghaibepbadmhjbngnkfehichf

# Development
## Firefox
- open `about:debugging` and load `manifest.json`

## Chrome
- open `chrome://extensions/` and enable developer mode
- click on load unpacked and select the source folder

# Documentation
https://swanalekha.smc.org.in/
