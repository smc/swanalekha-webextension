var browser = browser || chrome;

class SwanalekhaWebExtension extends Swanalekha {
	constructor(element, options) {
		super(element, options);
		this.element.addEventListener('swanalekha-toggle', this.toggle.bind(this));
	}

	onEnable() {
		super.onEnable();
		browser.runtime.sendMessage(true);
		this.element.style.borderLeft = '4px solid red';
	}

	onDisable() {
		super.onDisable();
		browser.runtime.sendMessage(false);
		this.element.style.borderLeft = 'none';
	}
}

function attach(elem) {
	if (!elem.dataset.swanalekha) {
		new SwanalekhaWebExtension(elem);
		elem.dataset.swanalekha = true;
	}
}

function scanAndInitialize() {
	// Attach swanalekha to input boxes
	let inputs = [...document.getElementsByTagName('input')];
	inputs.forEach((elem) => {
		let type = elem.getAttribute('type');
		if (type == 'text' || type == 'search') {
			attach(elem);
		}
	});

	// Attach swanalekha to text areas
	let textarea = [...document.getElementsByTagName('textarea')];
	textarea.forEach((elem) => attach(elem));

	// Attach swanalekha to contenteditables
	let ce = document.querySelectorAll('[contenteditable]');
	ce.forEach((elem) => attach(elem));
}

browser.runtime.onMessage.addListener(() => {
	let activeElement = document.activeElement;
	if (!activeElement.dataset.swanalekha) {
		// New elements added to page?
		scanAndInitialize();
	}
	activeElement.dispatchEvent(new Event('swanalekha-toggle'));
});

scanAndInitialize();
