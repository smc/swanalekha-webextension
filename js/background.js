var browser = browser || chrome;

var setIcon = (isEnabled) => {
	let name;
	if (isEnabled) {
		name = 'enabled';
	} else {
		name = 'disabled';
	}
	browser.browserAction.setIcon({ path: `icons/icon-${name}.png` });
};

function toggle() {
	// send message to the current active tab
	browser.tabs.query({ active: true, currentWindow: true }, (tabs) => {
		let lastTabId = tabs[0].id;
		browser.tabs.sendMessage(lastTabId, 'toggle');
	});
}

browser.browserAction.onClicked.addListener(() => {
	toggle();
});

browser.commands.onCommand.addListener(function (command) {
	if (command == 'swanalekha-toggle') {
		toggle();
	}
});

browser.runtime.onMessage.addListener(setIcon);
